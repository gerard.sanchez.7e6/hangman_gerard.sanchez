package com.example.hangman

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.hangman.databinding.ActivityGameBinding
import com.example.hangman.databinding.ActivityMenuBinding

class Game_Activity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding: ActivityGameBinding

    val easyWords = listOf("JULIO", "CESTA", "GLOBO", "PALMA", "TRUCO")
    val mediumWords = listOf("EMPEZAR", "ARDILLA", "HOTELES", "RECOGER", "DIENTES")
    val hardWords = listOf("PUBLICADO", "SIBERIANO", "CENICEROS", "SELECCION", "LABORALES")

    val hangmanPhotos = listOf(
        R.drawable.game0,
        R.drawable.game1,
        R.drawable.game2,
        R.drawable.game3,
        R.drawable.game4,
        R.drawable.game5,
        R.drawable.game6,
        R.drawable.game7
    )

    lateinit var difficult : String
    lateinit var word: String
    lateinit var guion: MutableList<String>

    var aux = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        difficult = bundle?.getString("gameDifficult").toString()

        when (difficult) {
            "EASY" -> word = easyWords.random()
            "MEDIUM" -> word = mediumWords.random()
            "HARD" -> word = hardWords.random()
        }

        guion  = MutableList(word.length){ "_" }
        binding.wordGame.text = guion.toString().replace("[", "").replace(",", "").replace("]", "")


        binding.letterA.setOnClickListener(this)
        binding.letterB.setOnClickListener(this)
        binding.letterC.setOnClickListener(this)
        binding.letterD.setOnClickListener(this)
        binding.letterE.setOnClickListener(this)
        binding.letterF.setOnClickListener(this)
        binding.letterG.setOnClickListener(this)
        binding.letterH.setOnClickListener(this)
        binding.letterI.setOnClickListener(this)
        binding.letterJ.setOnClickListener(this)
        binding.letterK.setOnClickListener(this)
        binding.letterL.setOnClickListener(this)
        binding.letterM.setOnClickListener(this)
        binding.letterN.setOnClickListener(this)
        binding.letter.setOnClickListener(this)
        binding.letterO.setOnClickListener(this)
        binding.letterP.setOnClickListener(this)
        binding.letterQ.setOnClickListener(this)
        binding.letterR.setOnClickListener(this)
        binding.letterS.setOnClickListener(this)
        binding.letterT.setOnClickListener(this)
        binding.letterU.setOnClickListener(this)
        binding.letterV.setOnClickListener(this)
        binding.letterW.setOnClickListener(this)
        binding.letterX.setOnClickListener(this)
        binding.letterY.setOnClickListener(this)
        binding.letterZ.setOnClickListener(this)


    }
    var intents = 0
    var attemps = 1
    @SuppressLint("SetTextI18n")
    override fun onClick(p0: View?) {

        val button = p0 as Button
        val text = button.text
        button.visibility = View.INVISIBLE
        var checKWord = ""

        if (text in word){
            for (i in word.indices){
                if (text == word[i].toString()){
                    guion[i] = text.toString()
                    aux++
                }
            }
        }
        else{

            binding.playerAttemps.text = "ATTEMPS: $attemps"
            binding.imageView.setImageResource(hangmanPhotos[intents])
            intents++
            attemps++

        }

        for (i in guion){
            checKWord += i
            binding.wordGame.text = guion.toString().replace("[", "").replace(",", "").replace("]", "")

        }
        if (intents == 8 || aux==word.length){
            val intent = Intent(this, Result_Activity::class.java)
            intent.putExtra("attemps", intents)
            startActivity(intent)
        }


    }


}

