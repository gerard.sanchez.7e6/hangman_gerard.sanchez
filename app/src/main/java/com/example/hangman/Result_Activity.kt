package com.example.hangman

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.hangman.databinding.ActivityResultBinding
@SuppressLint("SetTextI18n")
class Result_Activity : AppCompatActivity() {
    lateinit var binding: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)



        val bundle: Bundle? = intent.extras
        val intents = bundle?.getInt("attemps")

        binding.playAgain.setOnClickListener {
            val intent = Intent(this, Game_Activity::class.java)
            startActivity(intent)
        }
        binding.menu.setOnClickListener {
            val intent = Intent(this,Menu_Activity::class.java)
            startActivity(intent)
        }
        if (intents == 8) {
            binding.congratulations.text ="YOU ARE A LOSER !!"
            binding.loserImage.visibility = View.VISIBLE
            binding.finalAttemps.text = "You don't have succeesed before $intents mistakes"

        }
      else{
            binding.congratulations.text = "CONGRATULATIONS !!"
            binding.congratulationImage.visibility = View.VISIBLE
            binding.finalAttemps.text = "You have succeesed after $intents mistakes"

        }



    }

}