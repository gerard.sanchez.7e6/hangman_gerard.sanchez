package com.example.hangman

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen

class spashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        splashScreen.setKeepOnScreenCondition { true }
        Handler().postDelayed({
            val intent = Intent(this, Menu_Activity::class.java)
            startActivity(intent)
            finish()
        }, 3000)




    }


}