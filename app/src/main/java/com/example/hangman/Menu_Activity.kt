package com.example.hangman

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.hangman.databinding.ActivityMenuBinding


class Menu_Activity : AppCompatActivity() {
    lateinit var binding: ActivityMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.play.setOnClickListener {
            val intent =Intent(this,Game_Activity::class.java)
            intent.putExtra("gameDifficult",binding.difficult.selectedItem.toString())
            startActivity(intent)

        }
        binding.help.setOnClickListener {
            val alertDialog: AlertDialog = AlertDialog.Builder(this@Menu_Activity).create()
            alertDialog.setTitle("Como se juega?")
            alertDialog.setMessage("El propio juego pone una palabra secreta, de la que se conoce el número de letras como pista de ayuda.\n\nEl jugador irá poniendo letras que aparecerán en la palabra secreta si ésta las contiene, en caso contrario contará como fallo.\n\nEl objetivo consiste en acertar la palabra secreta sin cometer más de seis fallos.\n\nPuede configurar la dificultad del juego antes de empezar a jugar.\n\nTE ATREVES A JUGAR?")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "EXIT",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
            alertDialog.show()

        }

    }

}